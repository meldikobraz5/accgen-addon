/*----------Steam header stuff----------*/
var HEADERS_TO_STRIP_LOWERCASE = [
	'content-security-policy',
	'x-frame-options',
	'access-control-allow-origin'
];

function setHeader(e) {
	var origin = new URL(e.originUrl || e.initiator).hostname;
	if (!origin.endsWith("cathook.club"))
		return;
	if (!e.responseHeaders)
		return;

	e.responseHeaders = e.responseHeaders.filter(function (header) {
		return HEADERS_TO_STRIP_LOWERCASE.indexOf(header.name.toLowerCase()) < 0;
	});

	var targeturl = new URL(e.url);
	var url = !(e.originUrl || e.initiator) ? new URL("https://accgen.cathook.club") : new URL(e.originUrl || e.initiator);
	e.responseHeaders.push({
		name: "Access-Control-Allow-Origin",
		value: targeturl.hostname == "mail-attachment.googleusercontent.com" ? "null" : url.origin
	});
	e.responseHeaders.push({
		name: "Access-Control-Allow-Credentials",
		value: "true"
	});

	return {
		responseHeaders: e.responseHeaders
	};
}

var isFirefox = typeof browser != "undefined";
var isChrome = !isFirefox;
var browser_api = isChrome ? chrome : browser;

// Strip/modify headers that stop us from interacting with the steam page
browser_api.webRequest.onHeadersReceived.addListener(setHeader, {
	// We don't have access to all of these by default, but we may get access in the future thru setupPermissions
	urls: ["https://store.steampowered.com/*", "https://mail.google.com/mail/*", "https://mail-attachment.googleusercontent.com/*"]
}, (isFirefox ? ["blocking", "responseHeaders"] : ["blocking", "responseHeaders", "extraHeaders"]));
/*----------Steam header stuff end----------*/

const gmailpermissions = {
	origins: ["https://mail.google.com/*", "https://mail-attachment.googleusercontent.com/*"]
};

/*----------Addon communication----------*/
function sendReply(port, request, data) {
	port.postMessage({ id: request.id, data: data });
}

// Handle long-term comms with accgen content scripts
browser_api.runtime.onConnect.addListener(function (port) {
	port.onMessage.addListener(async function (request) {
		switch (request.data.task) {
			case "setupGmail":
				if (await setupPermissions(port, request)) {
					// Now that we have access to gmail, we need to readd the listener.
					browser_api.webRequest.onHeadersReceived.removeListener(setHeader);
					browser_api.webRequest.onHeadersReceived.addListener(setHeader, {
						urls: ["https://store.steampowered.com/*", "https://mail.google.com/mail/*", "https://mail-attachment.googleusercontent.com/*"]
					}, (isFirefox ? ["blocking", "responseHeaders"] : ["blocking", "responseHeaders", "extraHeaders"]));
					console.log("added listener")
					sendReply(port, request, { success: true });
				}
				break;
			case "version":
				// Tell the website about our version, may be important for backwards compat in the future
				sendReply(port, request, { version: browser_api.runtime.getManifest().version, apiversion: 2 });
				break;
			default:
				break;
		}
		return true;
	});
});
/*----------Addon communication end----------*/

/*----------Gmail support----------*/
function setupPermissions(port, request) {
	return new Promise(async function (resolve) {
		if (isFirefox && await browser_api.permissions.contains(gmailpermissions))
			resolve(true);
		else if (isChrome)
			browser_api.permissions.request(gmailpermissions, function (granted) {
				// Check if user approved the permissions
				if (granted)
					resolve(true);
				else {
					sendReply(port, request, { success: false, reason: "Addon permissions were denied." })
					resolve(false);
				}
			});
		else {
			let createData = {
				type: "detached_panel",
				url: "permissions.html",
				width: 250,
				height: 100
			};
			browser.windows.create(createData).then(function (window) {
				browser.windows.onRemoved.addListener(async (windowId) => {
					if (window.id == windowId)
						if (!await browser_api.permissions.contains(gmailpermissions)) {
							sendReply(port, request, { success: false, reason: "Addon permissions were denied." });
							resolve(false);
						}
						else
							resolve(true);
				});
			});
		}
	});
}
/*----------Gmail support end----------*/